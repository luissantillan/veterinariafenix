import React from 'react'
import { Container, Col, Row } from 'react-bootstrap'

export default function CL_FinishPurchase() {

    return (
        <Container fluid>
            <Row>
                <Col className="mt-5">
                    <h1>Gracias por tu Compra. Nos contactaremos a la brevedad para confirmar los Datos Ingresados!</h1>
                    <h3>Si abonaste con Mercado Libre. Contactate con Nosotros a través de Nuestro WhatsApp</h3>
                </Col>
            </Row>
        </Container>
    )
}